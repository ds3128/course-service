#!/bin/bash

namespace=${CLIENT_REF}-test

# Vérifier si le namespace existe déjà
if kubectl get namespace "$namespace" &>/dev/null; then
    echo "The namespace '$namespace' already exist."
else
    # Créer le namespace s'il n'existe pas
    kubectl create namespace "$namespace"
    echo "The namespace '$namespace' have been created."
fi
