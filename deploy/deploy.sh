#!/usr/bin/env bash
echo "starting deployment..."
kubectl apply -f ./deploy/deployment.yml

echo "starting service..."
kubectl apply -f ./deploy/service.yml

echo "starting ingress..."
kubectl apply -f ./deploy/ingress.yml
