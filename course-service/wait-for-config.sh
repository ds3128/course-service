#!/bin/sh

echo "Waiting for config-service to be ready..."

until curl -sSf "http://config-service:9999/actuator/health" > /dev/null; do
    echo "Config service is unavailable - sleeping"
    sleep 10
done

echo "Config service is up - starting config-service"
java -jar /app/app.jar
