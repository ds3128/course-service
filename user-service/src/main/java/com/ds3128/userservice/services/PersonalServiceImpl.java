package com.ds3128.userservice.services;

import com.ds3128.userservice.client.CourseRestClient;
import com.ds3128.userservice.common.dtos.PaginateDTO;
import com.ds3128.userservice.common.types.PaginateResponse;
import com.ds3128.userservice.dtos.StudentDtoRequest;
import com.ds3128.userservice.dtos.TeacherDtoRequest;
import com.ds3128.userservice.entities.Personal;
import com.ds3128.userservice.entities.Student;
import com.ds3128.userservice.entities.Teacher;
import com.ds3128.userservice.exceptions.PersonalAlreadyExistException;
import com.ds3128.userservice.exceptions.PersonalNotFoundException;
import com.ds3128.userservice.mappers.PersonalMapperImpl;
import com.ds3128.userservice.model.Course;
import com.ds3128.userservice.model.Mark;
import com.ds3128.userservice.repositories.PersonalRepository;
import com.ds3128.userservice.repositories.StudentRepository;
import com.ds3128.userservice.repositories.TeacherRepository;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
@Slf4j
public class PersonalServiceImpl implements PersonalService {

    private final PersonalRepository personalRepository;
    private final PersonalMapperImpl personalMapper;
    private final TeacherRepository teacherRepository;
    private final StudentRepository studentRepository;
    private final CourseRestClient courseRestClient;
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonalServiceImpl.class);

    public PersonalServiceImpl(PersonalRepository personalRepository, PersonalMapperImpl personalMapper, TeacherRepository teacherRepository, StudentRepository studentRepository, CourseRestClient courseRestClient) {
        this.personalRepository = personalRepository;
        this.personalMapper = personalMapper;
        this.teacherRepository = teacherRepository;
        this.studentRepository = studentRepository;
        this.courseRestClient = courseRestClient;
    }

    @Override
    public Student createStudent(StudentDtoRequest studentDtoRequest) {
        log.info("Attempting to create a new student with email: {}", studentDtoRequest.getEmail());

        if (!studentDtoRequest.getEmail().contains(".") || !studentDtoRequest.getEmail().contains("@")) {
            log.error("Invalid email format: {}", studentDtoRequest.getEmail());
            throw new RuntimeException("Invalid email");
        }

        Optional<Personal> personalOptional = this.personalRepository.findByEmail(studentDtoRequest.getEmail());

        if (personalOptional.isPresent()) {
            log.warn("Student creation failed: email {} already exists", studentDtoRequest.getEmail());
            throw new PersonalAlreadyExistException("this email already exist");
        }

        if (studentDtoRequest.getLastName().isBlank() || studentDtoRequest.getLastName().isEmpty()) {
            log.error("Student creation failed: LastName can't be null");
            throw new IllegalArgumentException("LastName can't be null");
        }
        if (studentDtoRequest.getPhoneNumber().isBlank() || studentDtoRequest.getPhoneNumber().isEmpty()) {
            log.error("Student creation failed: Phone number can't be null");
            throw new IllegalArgumentException("Phone number can't be null");
        }

        Student student = this.personalMapper.convertToStudent(studentDtoRequest);
        student.setIdStudent(UUID.randomUUID().toString());

        Student savedStudent = this.personalRepository.save(student);
        log.info("Successfully created a new student with ID: {}", savedStudent.getIdUser());
        return savedStudent;
    }

    @Override
    public Teacher createTeacher(TeacherDtoRequest teacherDtoRequest) {
        log.info("Attempting to create a new teacher with email: {}", teacherDtoRequest.getEmail());

        if (!teacherDtoRequest.getEmail().contains(".") || !teacherDtoRequest.getEmail().contains("@")) {
            log.error("Invalid email format: {}", teacherDtoRequest.getEmail());
            throw new RuntimeException("Invalid email");
        }

        Optional<Personal> personalOptional = this.personalRepository.findByEmail(teacherDtoRequest.getEmail());

        if (personalOptional.isPresent()) {
            log.warn("Teacher creation failed: email {} already exists", teacherDtoRequest.getEmail());
            throw new PersonalAlreadyExistException("this email already exist");
        }

        if (teacherDtoRequest.getLastName().isBlank() || teacherDtoRequest.getLastName().isEmpty()) {
            log.error("Teacher creation failed: LastName can't be null");
            throw new IllegalArgumentException("LastName can't be null");
        }
        if (teacherDtoRequest.getPhoneNumber().isBlank() || teacherDtoRequest.getPhoneNumber().isEmpty()) {
            log.error("Teacher creation failed: Phone number can't be null");
            throw new IllegalArgumentException("Phone number can't be null");
        }

        Teacher teacher = this.personalMapper.convertToTeacher(teacherDtoRequest);
        Teacher savedTeacher = this.personalRepository.save(teacher);
        log.info("Successfully created a new teacher with ID: {}", savedTeacher.getIdUser());
        return savedTeacher;
    }

    @Override
    public Student updateStudent(Long idUser, StudentDtoRequest studentDtoRequest) {
        log.info("Attempting to update student with ID: {}", idUser);

        Student studentExist = this.studentRepository.findById(idUser).orElseThrow(() -> {
            log.error("Student update failed: student not found with ID: {}", idUser);
            return new PersonalNotFoundException("student not found");
        });

        if (studentDtoRequest.getPhoneNumber() == null || studentDtoRequest.getPhoneNumber().isEmpty()) {
            studentDtoRequest.setPhoneNumber(studentExist.getPhoneNumber());
        }
        if (studentDtoRequest.getLastName() == null || studentDtoRequest.getLastName().isEmpty()) {
            studentDtoRequest.setLastName(studentExist.getLastName());
        }
        if (studentDtoRequest.getFirstName() == null || studentDtoRequest.getFirstName().isEmpty()) {
            studentDtoRequest.setFirstName(studentExist.getFirstName());
        }
        if (studentDtoRequest.getEmail() == null || studentDtoRequest.getEmail().isEmpty()) {
            studentDtoRequest.setEmail(studentExist.getEmail());
        }

        Student student = this.personalMapper.convertToStudent(studentDtoRequest);
        student.setIdUser(idUser);

        Student updatedStudent = personalRepository.save(student);
        log.info("Successfully updated student with ID: {}", updatedStudent.getIdUser());
        return updatedStudent;
    }

    @Override
    public Teacher updateTeacher(Long idUser, TeacherDtoRequest teacherDtoRequest) {
        log.info("Attempting to update teacher with ID: {}", idUser);

        Teacher teacherExist = this.teacherRepository.findById(idUser).orElseThrow(() -> {
            log.error("Teacher update failed: teacher not found with ID: {}", idUser);
            return new PersonalNotFoundException("Teacher not found");
        });

        if (teacherDtoRequest.getPhoneNumber() == null || teacherDtoRequest.getPhoneNumber().isEmpty()) {
            teacherDtoRequest.setPhoneNumber(teacherExist.getPhoneNumber());
        }
        if (teacherDtoRequest.getLastName() == null || teacherDtoRequest.getLastName().isEmpty()) {
            teacherDtoRequest.setLastName(teacherExist.getLastName());
        }
        if (teacherDtoRequest.getFirstName() == null || teacherDtoRequest.getFirstName().isEmpty()) {
            teacherDtoRequest.setFirstName(teacherExist.getFirstName());
        }
        if (teacherDtoRequest.getEmail() == null || teacherDtoRequest.getEmail().isEmpty()) {
            teacherDtoRequest.setEmail(teacherExist.getEmail());
        }
        if (teacherDtoRequest.getSpecialization() == null || teacherDtoRequest.getSpecialization().isEmpty()) {
            teacherDtoRequest.setSpecialization(teacherExist.getSpecialization());
        }

        Teacher teacher = this.personalMapper.convertToTeacher(teacherDtoRequest);
        teacher.setIdUser(idUser);

        Teacher updatedTeacher = this.personalRepository.save(teacher);
        log.info("Successfully updated teacher with ID: {}", updatedTeacher.getIdUser());
        return updatedTeacher;
    }

    @Override
    public Student findOneStudentById(Long idUser) {
        log.info("Fetching student with ID: {}", idUser);

        Student student = this.studentRepository.findById(idUser).orElseThrow(() -> {
            log.error("Student not found with ID: {}", idUser);
            return new PersonalNotFoundException("student not found");
        });

        List<Mark> markList;
        try {
            markList = this.courseRestClient.findAllMarkByIdUser(idUser);
        } catch (Exception e) {
            log.error("Failed to fetch marks for student ID {}: {}", idUser, e.getMessage(), e);
            throw new RuntimeException("Failed to fetch student marks");
        }

        student.setMarkList(markList);
        log.info("Successfully fetched student with ID: {}", student.getIdUser());
        return student;
    }

    @Override
    public Student findOneStudent(Long idUser) {
        log.info("Fetching student for other service with ID: {}", idUser);
        return this.studentRepository.findById(idUser).orElseThrow(() -> {
            log.error("Student not found for other service with ID: {}", idUser);
            return new PersonalNotFoundException("student not found");
        });
    }

    @Override
    public Teacher findOneTeacher(Long idUser) {
        log.info("Fetching teacher for other service with ID: {}", idUser);
        return this.teacherRepository.findById(idUser).orElseThrow(() -> {
            log.error("Teacher not found for other service with ID: {}", idUser);
            return new PersonalNotFoundException("Teacher not found");
        });
    }

    @Override
    public Teacher findOneTeacherById(Long idUser) {
        log.info("Fetching teacher with ID: {}", idUser);

        Teacher teacher = this.teacherRepository.findById(idUser).orElseThrow(() -> {
            log.error("Teacher not found with ID: {}", idUser);
            return new PersonalNotFoundException("Teacher not found");
        });

        List<Course> courseList;
        try {
            courseList = this.courseRestClient.findAllCourseByIdUser(idUser);
        } catch (Exception e) {
            log.error("Failed to fetch courses for teacher ID {}: {}", idUser, e.getMessage(), e);
            throw new RuntimeException("Failed to fetch teacher courses");
        }

        teacher.setCourseList(courseList);
        log.info("Successfully fetched teacher with ID: {}", teacher.getIdUser());
        return teacher;
    }

    @Override
    public PaginateResponse<Teacher> findAllTeacher(PaginateDTO paginateDTO) {
        log.info("Fetching all teachers with pagination offset: {} and limit: {}", paginateDTO.getOffset(), paginateDTO.getLimit());

        PageRequest pageRequest = PageRequest.of(paginateDTO.getOffset(), paginateDTO.getLimit(), Sort.by("specialization").ascending());
        Page<Teacher> teacherPage = this.teacherRepository.findAll(pageRequest);

        List<Teacher> teacherDtoRequestList = teacherPage.stream().toList();
        long totalElements = teacherPage.getTotalElements();
        long totalPage = teacherPage.getTotalPages();
        long currentPage = teacherPage.getNumber();
        boolean hasMore = paginateDTO.getOffset() + paginateDTO.getLimit() < totalElements;

        log.info("Successfully fetched {} teachers, total elements: {}", teacherDtoRequestList.size(), totalElements);
        return new PaginateResponse<>(totalElements, hasMore, teacherDtoRequestList, currentPage, totalPage);
    }

    @Override
    public PaginateResponse<Student> findAllStudent(PaginateDTO paginateDTO) {
        log.info("Fetching all students with pagination offset: {} and limit: {}", paginateDTO.getOffset(), paginateDTO.getLimit());

        PageRequest pageRequest = PageRequest.of(paginateDTO.getOffset(), paginateDTO.getLimit(), Sort.by("lastName").ascending());
        Page<Student> studentPage = this.studentRepository.findAll(pageRequest);

        List<Student> studentDtoRequestList = studentPage.stream().toList();
        long totalElements = studentPage.getTotalElements();
        long totalPage = studentPage.getTotalPages();
        long currentPage = studentPage.getNumber();
        boolean hasMore = paginateDTO.getOffset() + paginateDTO.getLimit() < totalElements;

        log.info("Successfully fetched {} students, total elements: {}", studentDtoRequestList.size(), totalElements);
        return new PaginateResponse<>(totalElements, hasMore, studentDtoRequestList, currentPage, totalPage);
    }

    @Override
    public Student deleteStudentById(Long idUser) {
        log.info("Attempting to delete student with ID: {}", idUser);

        Student studentExist = this.studentRepository.findById(idUser).orElseThrow(() -> {
            log.error("Student deletion failed: student not found with ID: {}", idUser);
            return new PersonalNotFoundException("student not found");
        });

        this.studentRepository.deleteById(idUser);
        log.info("Successfully deleted student with ID: {}", idUser);
        return studentExist;
    }

    @Override
    public Teacher deleteTeacherById(Long idUser) {
        log.info("Attempting to delete teacher with ID: {}", idUser);

        Teacher teacherExist = this.teacherRepository.findById(idUser).orElseThrow(() -> {
            log.error("Teacher deletion failed: teacher not found with ID: {}", idUser);
            return new PersonalNotFoundException("Teacher not found");
        });

        this.teacherRepository.deleteById(idUser);
        log.info("Successfully deleted teacher with ID: {}", idUser);
        return teacherExist;
    }
}
