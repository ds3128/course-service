#!/bin/sh

#is_deployment_pod_ready() {
#    deployment_name="$1"
#    namespace="$2"
#
#    pod_name=$(kubectl get pods -n "$namespace" --selector=app="$deployment_name" -o jsonpath='{.items[0].metadata.name}')
#
#    if [ -z "$pod_name" ]; then
#        echo "Error: No pod found for deployment '$deployment_name' in namespace '$namespace'."
#        return 1
#    fi
#
#    echo "Waiting for pod '$pod_name' associated with deployment '$deployment_name' to be ready..."
#
#    until kubectl get pod "$pod_name" -n "$namespace" --output=jsonpath='{.status.conditions[?(@.type=="Ready")].status}' | grep -q "True"; do
#        echo "Pod '$pod_name' is not ready - sleeping"
#        sleep 10
#    done
#
#    echo "Pod '$pod_name' is ready"
#    sleep 60
#}

echo "Waiting for config-service to be ready..."

while ! curl -sSf "http://config-service:9999/actuator/health" > /dev/null; do
    echo "Config service is unavailable - sleeping"
    sleep 10
done

echo "Config service is up - starting fieldStudy-service"

# Utiliser la fonction pour vérifier et attendre que le pod du déploiement soit prêt
#deployment_name="field-database"
#namespace="default"
#
#is_deployment_pod_ready "$deployment_name" "$namespace"

# Démarrer le service fieldStudy-service une fois que les dépendances sont démarrées
echo "Starting fieldStudy-service..."
java -jar /app/app.jar
