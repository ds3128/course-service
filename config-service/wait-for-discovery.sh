#!/bin/sh

echo "Waiting for discovery-service to be ready..."

until curl -sSf "http://discovery-service:8761/actuator/health" > /dev/null; do
    echo "Discovery service is unavailable - sleeping"
    sleep 10
done

echo "Discovery service is up - starting config-service"
java -jar /app/app.jar
